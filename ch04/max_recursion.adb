-- max_recursion.adb:

with Ada.Task_Identification;
with Ada.Numerics.Discrete_Random;
with Ada.Numerics;
with Ada.Text_IO;
with Ada.Integer_Text_IO;

procedure max_recursion is
  type IntArray is array (Positive range <>) of Integer;
  ListOfInts : IntArray(1 .. 40);

  function generate_random_int(
    Min : in Integer;
    Max : in Integer)
      return Integer is
  begin
    -- if the min is not less than the max, then terminate this process.
    if (Min >= Max)
    then
      Ada.Task_Identification.Abort_Task(Ada.Task_Identification.Current_Task);
    end if;

    -- now that it is certain that the correct limits are observed, proceed to generate a random value within those limits.
    declare
      subtype Vals is Integer range Min .. Max;
      package CustomRandom is new Ada.Numerics.Discrete_Random(Result_Subtype => Vals);

      Gen : CustomRandom.Generator;
      GeneratedNum : Vals := 1;
    begin
      CustomRandom.Reset(Gen => Gen);
      GeneratedNum := CustomRandom.Random(Gen => Gen);

      return Integer(GeneratedNum);
    end;
  end generate_random_int;

  -- get the largest number in the entire array using recursion.
  function max_num(
    List : in IntArray;
    Largest : in Integer;
    Index : in Integer)
      return Integer is
  begin
    -- if the index is larger than the length of the array, then we have reached the end of the recursion and should now return.
    if Index > List'Length
    then
      return Largest;
    end if;

    -- if the largest value so far is smaller than what is found in the array, then replace this value in the next function call.
    if Largest < List(Index)
    then
      return max_num(List, List(Index), Index + 1);
    end if;

    -- if reached this far, then that means the current largest value has not been exceeded.
    return max_num(List, Largest, Index + 1);
  end max_num;
begin
  -- populate our array with random values.
  for index in ListOfInts'Range
  loop
    ListOfInts(index) := generate_random_int(1, 50);
  end loop;

  -- find the largest value using recurion.
  Ada.Text_IO.Put_Line(" The largest number: " & Integer'Image(max_num(ListOfInts, 0, 1)));
end max_recursion;

