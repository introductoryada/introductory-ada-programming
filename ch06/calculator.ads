-- calculator.ads:

--pragma Assertion_Policy(Check);

with Ada.Text_IO;

package calculator is
  -- this is for addition.
  function Addition(
    Input1 : in Integer;
    Input2 : in Integer)
      return Integer;

  -- this is for subtraction.
  function Subtraction(
    Input1 : in Integer;
    Input2 : in Integer)
      return Integer;

  -- this is for multiplication.
  function Multiplication(
    Input1 : in Integer;
    Input2 : in Integer)
      return Integer;

  -- this is for division.
  function Division(
    Input1 : in Integer;
    Input2 : in Integer)
      return Integer;
    --with pre => Input2 /= 0;
end calculator;

